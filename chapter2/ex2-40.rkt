#lang sicp

; definitions

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence) (accumulate op initial (cdr sequence)))))

(define (enumerate-interval low high)
  (if (> low high)
      nil
      (cons low (enumerate-interval (+ low 1) high))))

(define (flatmap proc seq)  ; takes a list of lists, maps over it then flattens one level
  (accumulate append nil (map proc seq)))

(define (square x) (* x x))

(define (smallest-divisor n) (find-divisor n 2))
(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))
(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (prime-sum? pair)
  (prime? (+ (car pair) (cadr pair))))

(define (filter predicate sequence)
  (cond ((null? sequence) nil)
        ((predicate (car sequence))
         (cons (car sequence)
               (filter predicate (cdr sequence))))
        (else (filter predicate (cdr sequence)))))

(define (make-pair-sum pair)
  (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))

; given int n, generate sequence of pairs (i, j) with 1 <= j < i <= n

; unique pairs alg
; For each int i <= n, enumerate the integers j < i
;   And for each such i and j generate the pair (i, j)

; domain: integer n
; range: sequence of integer pairs (i, j) such that 1 <= j < i <= n
; interval [1, n]
; given the interval 1 <= j < i <= n:
;   for each int i <= n, enum ints j < i
;     for each such i and j, return (i, j)

; we have two intervals: j < i <= n and 1 <= j < i

; DOMAIN AND RANGE is key to understand unique-pairs:
;   1. (enumerate-interval low high) returns nil (the empty list) if high < low like with the first call
;      (enumerate-interval 1 (- i 1)) where i = 1
;   2. (map proc sequence) returns nil if the sequence provided is nil. This is how i > 1 in the
;      requirement 1 <= j < i <= n is enforced: enumerate-interval returns nil for i = 1 and map
;      returns nil for a nil sequence. flatmap then flattens the nil sublist out of the final return
;      list

(define (unique-pairs n)
  (flatmap (lambda (i) (map (lambda (j) (list i j)) (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 n)))

(define (prime-sum-pairs n)
  (map make-pair-sum
       (filter prime-sum? (unique-pairs n))))

; test

(prime-sum-pairs 6)
