#lang sicp

(define (leaf? object) (eq? (car object) 'leaf))
(define (weight-leaf x) (caddr x))
(define (make-leaf symbol weight) (list 'leaf symbol weight))
(define (symbol-leaf x) (cadr x))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))

(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair) ; symbol
                               (cadr pair)) ; frequency
                    (make-leaf-set (cdr pairs))))))

(define (make-code-tree left right) ;; returns 4-tuple
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))

(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

;; implement procedure successive-merge
;; problem: use make-code-tree to successively merge the smallest-weight elements of the set until
;; there is only one element left, which is the desired huffman tree
;;   solution:
;;     this is what we want to do visually:
;;     (D C B A) <-- we start with an ordered list (smallest to largest)
;;     ((C D) B A) <-- combine 2nd element with 1st element. add rest of list to the end
;;     ((B (C D)) A) <-- combine 2nd element with 1st element. add rest of list to the end
;;     (A (B (C D))) <-- repeat above step until no more elements

;;     since the elements are ordered from smallest to largest, we merge the 2nd and 1st elements
;;     using make-code-tree, and then append the rest of the list to the end and then repeat until
;;     there are no more elements left. this builds the desired huffman tree in the first
;;     element of the list successively. the remaining elements in the list are the ordered leaves
;;     that are yet to be combined into the huffman tree, which we draw from to build the huffman
;;     tree until there are none left.

;; successive-merge
;; param: ordered set of leaves of the form: ((leaf D 1) (leaf C 1) (leaf B 2) (leaf A 4))
;; return value: huffman tree of form: 
;;   ((leaf A 4) ((leaf B 2) ((leaf D 1) (leaf C 1) (D C) 2) (B D C) 4) (A B D C) 8)
;;   each node is a 4-tuple: (left-branch right-branch symbol-set weight)

(define (get-1st-elem l)
  (car l))

(define (get-2nd-elem l)
  (cadr l))

(define (get-rest-of-list l)
  (cddr l))

(define (rearrange l)
  (cons (make-code-tree (get-2nd-elem l) (get-1st-elem l)) (get-rest-of-list l)))

(define (successive-merge ordered-leaves)
  (if (null? (cdr ordered-leaves))
      ordered-leaves
      (successive-merge (rearrange ordered-leaves))))

;; test

(define pairs '((A 8) (B 4) (C 2) (D 1)))
(generate-huffman-tree pairs)
