#lang sicp

; - coordinates in the unit square are used to specify images
; - for each frame, we associate a frame coordinate map which will be used to
;   shift and scale images to fit the frame
; - a frame coordinate map maps a vector within the unit vector to the frame,
;     - so for example if we have a weird parallelogram frame and we want to
;       get its midpoint, we can pass say the vector (0.5, 0.5) (the
;       middle of the unit square) to the parallelogram's frame coordinate
;       map, which will transform (0.5, 0.5) to whatever the midpoint of the
;       parallelogram

; frame-coord-map: returns the frame coordinate map of the given frame,
; which is a procedure that given a vector within the unit square, returns
; a transformed vector. Maps unit square vectors to frame vectors
(define (frame-coord-map frame)
  (lambda (v)
    ; Origin(Frame) + x*Edge1(Frame) + y*Edge2(frame) where v = (x, y)
    (add-vect (origin-frame frame)
              (add-vect (scale-vect (xcor-vect v) (edge1-frame frame))
                   (scale-vect (ycor-vect v) (edge2-frame frame))))))
