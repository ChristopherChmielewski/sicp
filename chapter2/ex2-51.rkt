#lang sicp

(define (below painter1 painter2)
  (let ((split-point (make-vect 0.0 0.5)))
    (let ((paint-below
           (transform-painter
            painter1
            (make-vect 0.0 0.0)
            (make-vect 1.0 0.0)
            split-point
           (paint-above
            (transform-painter
             painter1
             split-point
             (make-vect 1.0 0.5)
             (make-vect 0.0 1.0))))
           (lambda (frame)
             (paint-below frame)
             (paint-above frame)))))))

(define (below-rotate painter1 painter2 rotate1 rotate2)
  (let ((split-point (make-vect 0.0 0.5)))
    (let ((paint-below
           (transform-painter
            painter1
            (make-vect 0.0 0.0)
            (make-vect 1.0 0.0)
            split-point
           (paint-above
            (transform-painter
             painter1
             split-point
             (make-vect 1.0 0.5)
             (make-vect 0.0 1.0))))
           (lambda (frame)
             ((rotate1 paint-below) frame)
             ((rotate2 paint-above) frame)))))))
