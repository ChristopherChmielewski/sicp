#lang sicp

; implement a data abstraction for vectors
;   create constructor make-vect
;   selectors xcor-vect and ycor-vect
;   procedures add-vect, sub-vect, scale-vect

; constructor
(define (make-vect xcor ycor)
  (cons xcor ycor))

; selectors
(define (xcor-vect v)
  (car v))

(define (ycor-vect v)
  (cdr v))

; vector operations

; addition: (x1, y1) + (x2, y2) = (x1 + x2, y1 + y2)
(define (add-vect v1 v2)
  (make-vect (+ (xcor-vect v1) (xcor-vect v2))
             (+ (ycor-vect v1) (ycor-vect v2))))

; subtraction: (x1, y1) - (x2, y2) = (x1 - x2, y1 - y2)
(define (sub-vect v1 v2)
  (make-vect (- (xcor-vect v1) (xcor-vect v2))
             (- (ycor-vect v1) (ycor-vect v2)))) 

; scaling: s * (x, y) = (sx, sy)
(define (scale-vect v s)
  (make-vect (* s (xcor-vect v)) (* s (ycor-vect v))))

; test

(define v (make-vect 1 2))
(define v1 (make-vect 2 1))
(define v2 (make-vect 5 3))

(display v) (newline)
(display (xcor-vect v)) (newline)
(display (ycor-vect v)) (newline)

(add-vect v1 v2)
(sub-vect v2 v1)
(scale-vect v1 2)
