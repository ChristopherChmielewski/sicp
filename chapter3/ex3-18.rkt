#lang sicp

(define (last-pair x)
  (if (null? (cdr x)) x (last-pair (cdr x))))

(define (make-cycle x)
  (set-cdr! (last-pair x) x)
  x)

(define z (make-cycle (list 'a 'b 'c)))

;; do this by collecting the already seen pairs in a seperate data structure, and when cdr'ing
;; through a list, if the cdr points to any of these stored pairs then we know we are in a cycle
(define (has-cycle l)
  ;; returns true if the list l contains a cycle. false otherwise
  (define (cycle-iter l encountered-pairs)
    (cond ((eq? l '()) #f)
          ((encountered-pair? (car l) encountered-pairs) #t)
          (else (cycle-iter (cdr l) (cons (car l) encountered-pairs)))))
  (define (encountered-pair? pair encountered-pairs)
    (if (eq? encountered-pairs '())
        #f
        (if (eq? pair (car encountered-pairs))
            #t
            (encountered-pair? pair (cdr encountered-pairs)))))
  (cycle-iter l '()))

;; test

(has-cycle (make-cycle '(a b c)))
(has-cycle '(a b c))
