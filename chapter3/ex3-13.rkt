#lang sicp

(define (last-pair x)
  (if (null? (cdr x)) x (last-pair (cdr x))))

(define (make-cycle x)
  (set-cdr! (last-pair x) x)
  x)

(define z (make-cycle (list 'a 'b 'c)))
(display z)

;; (last-pair z)

;; box and pointer diagram of z
;; z-->[|,-]-->[|,-]-->[|,|]
;; ^    |       |       | |
;; |    a       b       c |
;; |                      |
;; |-----------------------

;; (last-pair z) causes an inifite loop since the structure z has no empty list as its last
;; element. it instead has a pointer back it itself as its last element, so last-pair loops
;; forever.
